import React from "react";
import { withRouter } from "react-router-dom";
import "./login.component.css";
import { AuthenticationService as authService } from "../service/authentication.service";

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      submitted: false,
      username: "",
      password: "",
      requiredErrors: {
        username: true,
        password: true,
      },
    };

    this.onLogin = this.onLogin.bind(this);
    this.handleChange = this.handleChange.bind(this);
    authService.registerLogoutListener();
  }

  render() {
    return (
      <div id="login-container">
        <div className="card">
          <h4 className="card-header">Login</h4>
          <div className="card-body">
            <form>
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <input
                  className={`form-control ${
                    this.state.submitted && this.state.requiredErrors.username
                      ? "is-invalid"
                      : ""
                  }`}
                  id="username"
                  placeholder="Username"
                  type=" text"
                  value={this.state.username}
                  name="username"
                  onChange={this.handleChange}
                />
                {this.state.submitted && this.state.requiredErrors.username && (
                  <div className="invalid-feedback">
                    <div>Username is required</div>
                  </div>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  className={`form-control ${
                    this.state.submitted && this.state.requiredErrors.password
                      ? "is-invalid"
                      : ""
                  }`}
                  id="password"
                  placeholder="Password"
                  type="password"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
                {this.state.submitted && this.state.requiredErrors.password && (
                  <div className="invalid-feedback">
                    <div>Password is required</div>
                  </div>
                )}
              </div>
              <div className="form-group mt-3">
                <button
                  className="btn btn-primary"
                  onClick={this.onLogin}
                  disabled={this.state.loading}
                >
                  {this.state.loading && (
                    <span className="spinner-border spinner-border-sm mr-1" />
                  )}
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value,
      requiredErrors: {
        ...this.state.requiredErrors,
        [name]: value.length === 0,
      },
    });
  }

  onLogin() {
    event.preventDefault();
    this.setState({
      submitted: true,
    });
    if (!this.formValid()) {
      return;
    }

    this.setState({
      loading: true,
    });
    setTimeout(() => {
      authService.login({
        username: this.state.username,
        password: this.state.password,
      });
      this.setState({
        loading: false,
      });
      this.props.history.push("/dashboard");
    }, 1000);
  }

  formValid() {
    const errors = this.state.requiredErrors;
    return !errors.username && !errors.password;
  }
}

export default withRouter(Login);
